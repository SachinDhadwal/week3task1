package daoImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

// This class Will create the text file and write the log msg into the file
public class LogData {

    public String storeData(String msg){
        Path  path = Paths.get("MyLogFile.txt");

        Date date = new Date();
        String logDate = date.dateFormatter();
        String result = logDate.concat(msg+"\n");

        try {
            boolean fileExits = Files.exists(path);
            if(fileExits){
                Files.write(path,result.getBytes(),StandardOpenOption.APPEND);

            }
            else {
                Files.createFile(path);
                Files.write(path,result.getBytes());
            }

        }catch (IOException exception){
            exception.printStackTrace();
        }
        return msg;
    }

}

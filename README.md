# Coding Challenge day 5

Kindly go through the problem statement given below carefully and attempt the Challenge and Please submit if you want us to review

Thanks,
Program Office.
# Todo Manager Sprint 5

## Problem Statement

- Todo Manager is an application which can Manage our Tasks and keep track of our Tasks.
- Please use a text file to log the activities in the system.
- Please use date and time api to store date and time in the logs.
- Please use date api to store the date of the task as well
- Use Lambda expression to create the methods

## Instructions:-

- Please refactor the code created in sprint 4.
- Estimated completion time of the challenge will be 2hr.
- Make sure you're uploading the code on Olympus.